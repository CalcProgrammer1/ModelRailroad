#----------------------------------#
# Fast clock to bits               #
#----------------------------------#

import java
import java.beans
import jmri

#----------------------------------#
# Get JMRI Timebase instance       #
#----------------------------------#
timebase = jmri.InstanceManager.getDefault(jmri.Timebase)

#----------------------------------#
# Set initial time to zero         #
#----------------------------------#
prev_time = long(0)

class setStartup(jmri.jmrit.automat.AbstractAutomaton):
  
  def init(self):
    return

  def handle(self):
    global prev_time

    #----------------------------------#
    # Get time in seconds since Jan 1  #
    # 1970 (UNIX epoch)                #
    #----------------------------------#
    date = timebase.getTime().getTime() / 1000

    #----------------------------------#
    # Convert to seconds in day        #
    #----------------------------------#
    time = long((date % 86400) - 21600)

    #----------------------------------#
    # Convert to minutes               #
    #----------------------------------#
    time = time / 60

    #----------------------------------#
    # Loop through 16 bits in value and#
    # update associated DCC address if #
    # the bit has changed since last   #
    # update                           # 
    #----------------------------------#
    for bit in range(0,15):
        if ((time >> bit) & 1) == 0:
            if ((prev_time >> bit) & 1) != ((time >> bit) & 1):
                addr = 100 + bit
                turnouts.provideTurnout(str(addr)).setState(CLOSED)
        else:
            if ((prev_time >> bit) & 1) != ((time >> bit) & 1):
                addr = 100 + bit
                turnouts.provideTurnout(str(addr)).setState(THROWN)

    #----------------------------------#
    # Update previous time value       #
    #----------------------------------#
    prev_time = time

    #----------------------------------#
    # Delay 500 milliseconds           #
    #----------------------------------#
    self.waitMsec(500)

    #----------------------------------#
    # Returning TRUE indicates to keep #
    # running the handle() function    #
    #----------------------------------#
    return True

#----------------------------------#
# Start the script                 #
#----------------------------------#
setStartup().start()
