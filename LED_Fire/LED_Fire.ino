// LED Fire Effect

int ledPin1 = 9;
int ledPin2 = 10; //red wire
int ledPin3 = 11;

void setup() {
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
}

void loop() {
  analogWrite(ledPin1, random(45, 90));
  analogWrite(ledPin2, random(40, 115));  //red
  analogWrite(ledPin3, random(50, 125));  
  if(random(45) == 1){ 
       analogWrite(ledPin2, 235);
     }
  if(random(50) == 1){ 
       analogWrite(ledPin1, 255);
     }
  if(random(75) == 1){ 
       analogWrite(ledPin3, 255);
     }
  delay(random(120));
}
