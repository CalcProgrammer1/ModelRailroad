int ledPin = 2;  //white led
int ledBlue = 3; //blue led
int i;
int count;

void setup() 
{
pinMode(ledPin, OUTPUT);
pinMode(ledBlue, OUTPUT);
}

void loop() 
{
count=random(10,60);
for (i=0;i<count;i++)
{
  digitalWrite(ledPin, HIGH); //turn LED on
  digitalWrite(ledBlue, HIGH);
  delay(random(60));
    
  digitalWrite(ledPin, LOW);  // turn LEd off
  delay(random(15));
  digitalWrite(ledBlue, LOW);
  delay(random(200));
}
delay(random(800,2000));  //  wait between arcs
}
