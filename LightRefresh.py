#----------------------------------#
# Light refresher                  #
#----------------------------------#

import java
import java.beans
import jmri

class setStartup(jmri.jmrit.automat.AbstractAutomaton):
  
  def init(self):
    return

  def handle(self):
    #----------------------------------#
    # Delay 50 milliseconds            #
    #----------------------------------#
    self.waitMsec(500)

    for light in lights.getNamedBeanSet():
      light.setCommandedState(light.getCommandedState())
      self.waitMsec(15)

    #----------------------------------#
    # Returning TRUE indicates to keep #
    # running the handle() function    #
    #----------------------------------#
    return True

#----------------------------------#
# Start the script                 #
#----------------------------------#
setStartup().start()
