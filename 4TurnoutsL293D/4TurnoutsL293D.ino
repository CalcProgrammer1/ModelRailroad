//////////////////////////////////////////
//
// Small Layout turnout control
// Demonstration Sketch
//
//////////////////////////////////////////

////////////////////////////////////////
//
// Definitions
//
////////////////////////////////////////

// Motors 1, 2, 3, 4 are directly attached
#define MOTOR1A_PIN 2
#define MOTOR1B_PIN 3
#define MOTOR2A_PIN 4
#define MOTOR2B_PIN 5
#define MOTOR3A_PIN 6
#define MOTOR3B_PIN 7
#define MOTOR4A_PIN 8
#define MOTOR4B_PIN 9

////////////////////////////////////////
// Basic parameters
///////////////////////////////////////
#define NUMBER_OF_TURNOUTS 4


///////////////////////////////////////
// Data Structures
///////////////////////////////////////

//////////////////////////////////////
// TURNOUT_DEF holds all configuration
// information about turnouts and panel LEDS
//////////////////////////////////////
typedef struct TURNOUT_DEF {
  uint8_t button_pin_main; // Digital or analog pin for the button associated with this turnout  
  uint8_t button_pin_div; // Digital or analog pin for the button associated with this turnout
  uint8_t motor_id; // ID of motor
  uint8_t address; // DCC turnout address
};

/////////////////////////////////////
// TURNOUT_DATA is wrapper structure holding
// both configuration and runtime data for turnout operation
/////////////////////////////////////
typedef struct TURNOUT_DATA {
  TURNOUT_DEF data; // configuration
  byte alignment;
};

// Alignment state values
#define ALIGN_NONE 0
#define ALIGN_MAIN  1
#define ALIGN_DIVERGENT 2

//////////////////////////////////////////
//
// Global variables
//
//////////////////////////////////////////

//////////////////////////////////////////
// TURNOUT_DATA Array
// * A0, A1, etc refer to analog pins which are used for buttons in this example
// * Replace pos_main (93) and pos_div (117) with real values for each turnout
// * LEDS are identified by their output position in the shift register chain;
// the identifier is a number between 0 and (NUMBER_OF_SHIFT_REGISTERS * 8) - 1. 
// Example assumes LEDS are connected to shift register outputs sequentially 
// from the first output of first register. You can connect LEDS to any output in
// any order; just set the identifiers accordingly.
//
// Only the TURNOUT_DEF part of the TURNOUT_DATA structure has to be initialized here; 
// The remaining elements are managed internally and are initialized automatically
//////////////////////////////////////////

TURNOUT_DATA turnouts[NUMBER_OF_TURNOUTS] = {
  {{12, 13, 1, 10}},
  {{A0, A1, 2, 11}},
  {{A2, A3, 3, 12}},
  {{A4, A5, 4, 13}},
};

void setup() 
{  
  Serial.begin(115200);

  // Initialize the outputs
  pinMode(MOTOR1A_PIN, OUTPUT);
  pinMode(MOTOR1B_PIN, OUTPUT);
  pinMode(MOTOR2A_PIN, OUTPUT);
  pinMode(MOTOR2B_PIN, OUTPUT);
  pinMode(MOTOR3A_PIN, OUTPUT);
  pinMode(MOTOR3B_PIN, OUTPUT);
  pinMode(MOTOR4A_PIN, OUTPUT);
  pinMode(MOTOR4B_PIN, OUTPUT);
  
  // initialize each turnout 
  for(int i = 0; i < NUMBER_OF_TURNOUTS; i++){
    // set the pin mode for the button pin
    pinMode(turnouts[i].data.button_pin_main, INPUT_PULLUP);
    pinMode(turnouts[i].data.button_pin_div, INPUT_PULLUP);
    // initialize position
    setTurnout(i, ALIGN_MAIN);
    }
} // end of setup

void loop() 
{
  // loop through the turnouts array
  for(int i = 0; i < NUMBER_OF_TURNOUTS; i++){
     // if a turnout is NOT in motion, check to see if its button is pressed
     int button_state_main = digitalRead(turnouts[i].data.button_pin_main);
     int button_state_div = digitalRead(turnouts[i].data.button_pin_div);

     if(turnouts[i].data.button_pin_main > A5) button_state_main = ( analogRead(turnouts[i].data.button_pin_main) > 512 );
     if(turnouts[i].data.button_pin_div > A5) button_state_div = ( analogRead(turnouts[i].data.button_pin_div) > 512 );

     if(button_state_main == LOW && turnouts[i].alignment != ALIGN_MAIN)
     {
      setTurnout(i, ALIGN_MAIN);      
     }
     if(button_state_div == LOW && turnouts[i].alignment != ALIGN_DIVERGENT)
     {
      setTurnout(i, ALIGN_DIVERGENT); 
     }
  }

  process_serial();
}// end of main loop

////////////////////////////////////////////////////////////////
// Supporting Functions
////////////////////////////////////////////////////////////////

void setTurnout(int id, int align){
    // Set indicators to show turnout in motion
    turnouts[id].alignment = ALIGN_NONE;

    // Set values to trigger motion on next loop iteration
    switch(align){
        case ALIGN_MAIN:
          setMotor(turnouts[id].data.motor_id, HIGH, LOW);
          turnouts[id].alignment = ALIGN_MAIN;
          break;
        case ALIGN_DIVERGENT:
          setMotor(turnouts[id].data.motor_id, LOW, HIGH);
          turnouts[id].alignment = ALIGN_DIVERGENT;
          break;
      }
}

void setMotor(uint8_t id, uint8_t a_state, uint8_t b_state)
{
  switch(id)
  {
    case 1:
      digitalWrite(MOTOR1A_PIN, a_state);
      digitalWrite(MOTOR1B_PIN, b_state);
      break;

    case 2:
      digitalWrite(MOTOR2A_PIN, a_state);
      digitalWrite(MOTOR2B_PIN, b_state);
      break;

    case 3:
      digitalWrite(MOTOR3A_PIN, a_state);
      digitalWrite(MOTOR3B_PIN, b_state);
      break;

    case 4:
      digitalWrite(MOTOR4A_PIN, a_state);
      digitalWrite(MOTOR4B_PIN, b_state);
      break;
  }
}

char ser_buf[32];
char ser_idx = 0;

void process_serial()
{
  if(Serial.available())
  {
    ser_buf[ser_idx] = Serial.read();

    if(ser_buf[ser_idx] == ']')
    {
      ser_buf[ser_idx + 1] = '\0';
      int addr;
      int val;
      sscanf(ser_buf, "[%d,%d]", &addr, &val);

      for(int i = 0; i < NUMBER_OF_TURNOUTS; i++)
      {
        if(addr == turnouts[i].data.address)
        {
          if(val)
          {
            setTurnout(i, ALIGN_DIVERGENT);
          }
          else
          {
            setTurnout(i, ALIGN_MAIN);
          }
        }
      }

      ser_idx = 0;
    }
    else
    {    
      if(ser_idx == 0 && ser_buf[ser_idx] != '[')
      {
        ser_idx = 0;
      }
      else
      {
        ser_idx++;
      }
    }
  }
}
