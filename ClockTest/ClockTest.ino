#include <LiquidCrystal.h>

unsigned short time = 0;

LiquidCrystal lcd(2, 3, 4, 8, 7, 6, 5);

void setup() 
{  
  Serial.begin(115200);
}

unsigned long last_millis = 0;

void loop() 
{
  if(millis() - last_millis > 1000)
  {
    unsigned short hour = time / 60;
    unsigned short minute = time % 60;

    lcd.begin(16,2);
    lcd.print("clock time");
    lcd.setCursor(0,1);
    lcd.print(hour);
    lcd.setCursor(2,1);
    lcd.print(":");
    lcd.setCursor(3,1);
    lcd.print(minute);
  
    last_millis = millis();
  }

  process_serial();
}

char ser_buf[32];
char ser_idx = 0;

void process_serial()
{
  if(Serial.available())
  {
    ser_buf[ser_idx] = Serial.read();

    if(ser_buf[ser_idx] == ']')
    {
      ser_buf[ser_idx + 1] = '\0';
      int addr;
      int val;
      sscanf(ser_buf, "[%d,%d]", &addr, &val);

      if(addr >= 100 && addr <= 115)
      {
        if(val)
        {
          bitSet(time, addr-100);
        }
        else
        {
          bitClear(time, addr-100);
        }
      }

      ser_idx = 0;
    }
    else
    {    
      if(ser_idx == 0 && ser_buf[ser_idx] != '[')
      {
        ser_idx = 0;
      }
      else
      {
        ser_idx++;
      }
    }
  }
}
